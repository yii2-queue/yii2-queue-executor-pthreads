<?php

namespace yii\queue\executors\pthreads\threads;

use Yii;

/**
* 
*/
class Worker extends \Worker
{
    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function run()
    {
        Yii::$app = $this->app;
    }
}
