<?php

namespace yii\queue\executors\pthreads\threads;

use yii\queue\Job;

/**
* 
*/
class Work extends \Thread implements \Collectable
{
    public $job;
    public $return;
    public $exception;

    protected $complete = false;

    function __construct(Job $job)
    {
        $this->job = $job;
    }

    public function run()
    {
        try {
            $this->return = $this->job->execute();
        } catch (\Exception $e) {
            $this->exception = $e;
        }

        $this->setGarbage();
    }

    private $isGarbage = false;

    public function setGarbage()
    {
        $this->isGarbage = true;
    }

    public function isGarbage(): bool
    {
        return $this->isGarbage;
    }
}
