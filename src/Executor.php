<?php

namespace yii\queue\executors\pthreads;

use yii\queue\executors\pthreads\threads\Pool;
use yii\queue\executors\pthreads\threads\Worker;
use yii\queue\executors\pthreads\threads\Work;
use Kraken\Promise\Promise;
use Exception;
use Yii;

/**
 */
class Executor extends \yii\queue\executors\Executor
{
    private $_pool;

    /**
     * Max thread count
     */
    public $maxThreads = 10;

    /**
     * Worker class
     */
    public $workerClass = Worker::class;

    /**
     * @inheritdoc
     */
    public function handleMessage($message, $id = null, $ttr = null, $attempt = null)
    {
        $pool = $this->getPool();
        return new Promise(function ($resolve, $reject) use (&$message, &$pool) {
            $job = $this->getQueue()->getSerializer()->unserialize($message);
            $work = new Work($job);
            $pool->submit($work);
        });
    }

    /**
     * Gets the thread pool.
     * 
     * @return Pool
     */
    public function getPool()
    {
        if (!$this->_pool) {
            $this->_pool = new \Pool($this->maxThreads, $this->workerClass, [Yii::$app]);
        }
        return $this->_pool;
    }
}
