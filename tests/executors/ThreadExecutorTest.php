<?php

namespace tests\executors;

use tests\executors\jobs\SuccessJob;
use tests\executors\jobs\RejectJob;
use Exception;

/**
* 
*/
class ThreadExecutorTest extends TestCase
{
    /**
     * Gets the executor.
     *
     * @return     \yii\queue\executors\pthreads\Executor     The executor.
     */
    public function getExecutor()
    {
        return new \yii\queue\executors\pthreads\Executor([
            'queue' => $this->getQueue()
        ]);
    }


    public function testSuccessfulJob()
    {
        $executor = $this->getExecutor();

        $job = new SuccessJob([
            'return' => 'test'
        ]);

        $message = $this->getQueue()->getSerializer()->serialize($job);

        $executor->handleMessage($message)->then(function ($response) use (&$result) {
            $result = $response;
        });

        while($executor->getPool()->collect(function ($thread) {
            return $thread->isGarbage();
        }));


        $this->assertEquals('test', $actual);

        $executor->getPool()->shutdown();

    }

    public function testRejectedJob()
    {
        $executor = $this->getExecutor();

        $job = new RejectJob([
            'message' => 'test'
        ]);

        $message = $this->getQueue()->getSerializer()->serialize($job);

        $executor->handleMessage($message)->then(
            function ($result) {}, 
            function ($e) use (&$actual) {
                $actual = $e;
            }
        );

        $this->assertInstanceOf(Exception::class, $actual);
        $this->assertEquals('test', $actual->getMessage());
    }
}